###############################################################################
##Global File
##############################################################################

# Changelog

# 02 03 23 | David:
# - Changed ODA-Mittel to ODA-Leistungen
# - Changed EZ to ODA throughout dashboard (only exception: Fem EZ, since this is an established term)
# - added nicht verfügbar to femEZ time series plot
# - added disclaimer to sidebar

# 24 02 23 | David:
  # - changed scaling of per capita ODA data 
  # - changed sidebar labels "Gelder" to ODA terminology "Committed" "Grant Equivalent"
  # - added global function treemap_data_prep for treemap visualization (pie chart data prep remains in script but is unsused)
  # - added server function treemap_plot for visualizing ODA data in a treemap (plot pie chart remains ins script and is used for femEZ plots)
  # - added global function oda_timeseries_chart_data_prep for ODA sector time series data
  # - added server function sektoren_line for plotting ODA time series

# 23 02 2 | Lea: 
  # - finished some ot the TODOs (added "@David: Done" comments)
  # - still need to work on data_prep_file to get data for Boxes Aktuelle Lage

# 23 02 21 | Lea: 
  # - changed zoom in map_africa function
  # - changed file names of dataframe for data_boxes
# 23 02 17 | Lea:
  # - changed file names of dataframes (load() in lines 29-32)
# 23 02 16 | David: 
  #- changed variable names as described in server.r header comment
  #- all functions should now work with the ODA data set


##############################################################################
# Testing Summary: 
# SIDEBAR (INPUT MENU; LHS):
#   - All elements are displayed
#   -"Official Development Assistance" is should be written in two lines because exceeds borders of sidebar
#@David: Done (shorter title because linebreak regexes do not work)#TODO Insert line break (regex) for "Official Development Assistance" in sidebar 
#@Lea: Done (changed Legend to ODA terminonolgy) TODO Adjust Legend for ODA (Gelder belegt und gebucht) --> add legend title; change Memfis terminology to ODA terminology
#OPTIONAL (next steps)
#TODO (optional and will take more time): We could time series data for "Ern?hrungssituation", ACLED data and displacement data --> add possibility to select year/period 
#TODO (optional): Integrate other indicators (displacement, protest, violence, disaster) in sidebar, map and grafische Darstellung

# OUTPUT PAGE "AKTUELLE LAGE":
#   - TLH: Africa Map works for Ern?hrungssicherheit and Foodbasket.
#   - TRH: Tab "Graphische Dastellung" (bar plot) only works for foodbasket.
#   - TRH: Tab "Glossar" works but only refers to Ern?hrungssicherheit.
#   - TRH: Tab "Quellen" works.
#   - BOTTOM (from L to R): All 6 boxes are displayed but they only show when clicking a country on the Africa Map, except for internal displacement
#       - Nutrition data (left) works
#       - Foodbasket data works
#       - Disaster data does not work (only NA or 0)
#       - Internal Displacement does not work (no figures) --> @David: I think the problem is that our data contains two different displacement indicators - one with displacement for most recent years available (flow) and on for the total number of displaced persones (stock)
#       - ACLED violence: Many NA values displayed, only works for few countries (e.g. Ethiopia, South Africa)
#       - ACLED Protest (right): Many NA values displayed, only works for few countries (e.g. Ethiopia, South Africa)
#@David: Done #TODO Check function to create bar plot (TRH) for Ern?hrungssituation
#TODO Check function and data creating boxes for disaster, displacement, protest and violence (from line 371 in server.r file --> data_boxes_reac)
#@David: I think the problem with the boxes might result data wrangling (many missing values in displacement) or renaming some of the variables
#@David: Done #TODO Display Boxes with figures for "Africa" (regional averages/sums) if no country is clicked on map 

# OUTPUT PAGE "Official Development Assistance":
#   - TLH: Map works, but we could adjust the legend (definition of color bins)
#   - TRH: Pie Chart work; I replaced Kernthemen by sectors, but I think we should reduce number of sectors
#@Lea Done: The basic treemap functionality is in the script now. it is hardcoded for ODA data by sector though, we could still imporve flexibility of the code at some point #TODO: Tree Map for sectors instead of pie chart
#@Lea Done: fixed the bug where the landing page would show disaggregated national level data at first. now shows agregated data for all of Africa#TODO: correctly display aggregated data for all of Africa
#TODO: integrate ODA objectives as a hierarchical level or label into the treemap
#   - BLH: Belegung Haushaltsmittel works, but I am not sure whether method to compute figures is valid for ODA data
#   - BRF: Line graph for "Sektoren" does not wokr yet because too many categories (adjust in data_prep file and server file)
#           Line graph for "Fem. EZ" works.
#@Lea: Done, the line plots now work for sector groups. TODO: Time Serie for Sectors --> aggregate sectors
#TODO: harmomize color schemes for treemap and line charts: each sector group should have the same static color no matter the actual value
#TODO: make ODA line chart reactive to absolute vs per capita input

##############################################################################

#checks if pacman package exists, if not install
if (!require("pacman")) install.packages("pacman")

#install/load packages
pacman::p_load("shiny",
               "dplyr",
               "here",
               "leaflet",
               "shinyWidgets",
               "plotly",
               "shinydashboard",
               "rsconnect",
               "viridis",
               "rgeos",
               "sp"
)


#set wd
here::i_am("global.R")
#setwd("C:/Users/leasm/Dropbox/R Scripts/BMZ Posit/Afrika Dash")

#load data
load(here("data", "2023-02-17_data_crs.rdata"))
load(here("data", "2023-02-21_data_boxes_rev.rdata"))
load(here("data", "2023-02-17_map_data_rev.rdata"))
load(here("data", "ipc_glossar.rdata"))
#token <- read.csv(here("data", "shinyapps_token.csv"))

# drop columnc causing problems
#data_boxes <- data_boxes %>% select(-(names(data_boxes)[grep("\\.y", names(data_boxes))]))
#data_boxes <- data_boxes %>% select(-(names(data_boxes)[grep("\\.x", names(data_boxes))]))

################################################################################
###Functions
################################################################################

###################
#Plot Lage vor Ort
###################
# @Lea: I checked this function, it is written without using specific df and variable names. We should be able to use it as is. 
plot_lage_vor_ort <- function(data, variable, custom_title, hover_suffix= "", text_caption) {
  
  #remove 0 values
  data[data==0] <- NA
  
  #plot
  plotly::plot_ly(data = data,
                  x = ~.data[[variable]],
                  y = ~reorder(country_de, .data[[variable]]),
                  text = ~.data[[variable]],
                  texttemplate= paste("%{x}", hover_suffix),
                  textposition = 'auto',
                  type = 'bar',
                  #hoverinfo = 'y',          
                  hovertemplate = '%{y}: %{x}<extra></extra>',
                  orientation = 'h') %>% 
    
    #customise layout
    plotly::layout(
      title = list(
        text = custom_title,
        size = 20
      ),
      xaxis = list(
        title = "",
        zeroline = FALSE,
        showline = FALSE,
        showticklabels = FALSE,
        showgrid = FALSE
      ),
      yaxis = list(
        title = ""
      ),
      annotations = 
        list(x = 1,
             y = 0,
             text = paste0("Quelle: ", text_caption), 
             showarrow = FALSE,
             xref='paper',
             yref='paper', 
             xanchor='right',
             yanchor='auto',
             xshift=0, 
             yshift=0
        ),
      margin = 
        list(l = 50, r = 50,
             b = 50, t = 50,
             pad = 20)
    ) %>% 
    plotly::config(modeBarButtonsToRemove = c("zoomIn2d", "zoomOut2d", "resetScale2d", "pan2d", "zoom2d", "autoScale2d",
                                              "select2d", "lasso2d", "hoverCompareCartesian", "hoverClosestCartesian"), displaylogo = FALSE)
  
}

###################
#Plot Africa Map
###################
# @Lea: this function should also work as is, it does not use specific variable names
map_africa <- function(data, bins, displayed_var, labels, title, lab_form = " %", colorpalette, header) {
  
  #prepare colours
  pal <- leaflet::colorBin(
    #"YlOrRd", # red color map signifies high ODA = negative, which we do not want
    colorpalette, # this is more neutral and colorblind - friendly
    domain = displayed_var,
    bins = bins
  )
  
  #create leaflet object
  #@David I changed min Zoom as map could not be displayed entirely
  leaflet::leaflet(
    data = data,
    options = leafletOptions(
      #minZoom = 3.4,
      minZoom = 2,
      maxZoom = 7,
      dragging = TRUE
    )
  ) %>%
    
    #define view
    #@David I changed zoom as default zoom value was too large
    leaflet::setView(
      lng = 13.1021,
      lat = 0.2812,
      zoom = 2.5
      #zoom = 3.7
    ) %>%
    
    #polygon data
    leaflet::addProviderTiles(
      "MapBox",
      options = providerTileOptions(
        id = "mapbox.light",
        #TODO create environment variable with access token
        accessToken = Sys.getenv('MAPBOX_ACCESS_TOKEN')
      )
    ) %>% 
    
    #create coloured ploygons
    leaflet::addPolygons(
      fillColor = ~pal(displayed_var),
      layerId = ~ADMIN,
      weight = 2,
      opacity = 1,
      color = "white",
      dashArray = "3",
      fillOpacity = 0.7,
      
      #define what happens when mouse hovers over polygons
      highlightOptions = highlightOptions(
        weight = 5,
        color = "#666",
        dashArray = "",
        fillOpacity = 0.7,
        bringToFront = TRUE),
      
      #add labels
      label = labels,
      labelOptions = labelOptions(
        style = list("font-weight" = "normal", padding = "3px 8px"),
        textsize = "15px",
        direction = "auto")      
    ) %>% 

    #add legend
    leaflet::addLegend(
      pal = pal,
      values = ~displayed_var,
      opacity = 0.7,
      title = title,
      position = "bottomright",
      na.label = "Keine aktuellen Daten",
      labFormat = labelFormat(suffix = lab_form)
    ) %>%
    
    # @Lea: add title here
    leaflet::addControl(
      header,
      position = "topleft",
      className="map-title"
  )
}

###################
# prepare data for Pie Chart
###################

#@David: Change variable names to commitment and grant equivalent
# 
pie_chart_data_prep <- function(data, group_by_var) {
  
  data %>%
    dplyr::group_by({{group_by_var}}) %>%
    #dplyr::summarise(financial_commitment_in_mio_per_variable = sum(financial_commitment_in_mio))
    dplyr::summarise(financial_in_mio_per_variable = sum(financial_in_mio))
    
}


#@Lea: this is new
#############################################
# get meaningful sector labels for the plots 
#############################################
# we plot sector groups, ie roman numerals in the treemap
# to make the labels more inutitive than I or IV
# we have a function that replaces roman numerals with the verbal labels
# eg Social Infrastructure and Services 
optmimize_sectorgroup_labels <- function(labels) {

  labels = recode(labels,  I='I Soziale Infrastruktur und Dienste', 
                           II='II Wirtschaftliche Infrastruktur und Dienste',
                           III='III Produktionsbereiche',
                           IV='IV Multisektoral/Querschnitt',
                           VI = 'VI Warenhilfe und allgemeine Programmhilfe',
                           VII='VII Schuldenerleichterung',
                           VIII='VIII Humanitaere Hilfe',
                           IX='IX Sonstige Massnahmen')
  
}


############################
# prepare data for treemap 
############################

# treemap requires a long form df with id, label, parent, and value columns
# all hierarchical levels are stacked on top of each other and included in 
# the same df

# only used for testing purposes, remove before publication
#data = subset(data_bmz_group, country_en=='Algeria')
#data=data_bmz_group
#data$financial_in_mio = round(data$grant_equiv_in_mio, 2)

treemap_data_prep <- function(data, country) {
  
  # aggregate data if no country was chosen
  if (country=="Afrika") {
     data = aggregate(data, by=list(data$sector_name),FUN=mean)
     data$sector_name = data$Group.1
  }
  
  # pick the roman numerals out of the sector name to get sector_group variable
  data$sector_group = sapply(strsplit(data$sector_name, split='.', fixed=TRUE), `[`, 1) 
  
  # get relevant columns from input data
  #@Lea: I started working on adding objectives to the treemap, but haven't finished yet
  #dfwide = data[c("country_en","sector_group","sector_name","financial_in_mio","objective")]
  #dfwide$id = paste(dfwide$country_en,dfwide$sector_group,dfwide$sector_name,dfwide$objective, sep="|")
  dfwide = data[c("country_en","sector_group","sector_name","financial_in_mio")]
  dfwide$id = paste(dfwide$country_en,dfwide$sector_group,dfwide$sector_name, sep="|")
  
  # compute the data at the sector level (lowest level)
  #sectordata = aggregate(financial_in_mio ~ id * objective, data = dfwide, sum)
  sectordata = aggregate(financial_in_mio ~ id, data = dfwide, sum)
  names(sectordata) = c("id","value")
  # remove reduntant information from the label: country name and sectorgroup
  #sectordata$label= paste(sapply(strsplit(sectordata$id, split='|', fixed=TRUE), `[`, 3),sapply(strsplit(sectordata$id, split='|', fixed=TRUE), `[`, 4), sep=" | ")
  sectordata$label = sapply(strsplit(sectordata$id, split='|', fixed=TRUE), `[`, 3)#sectordata$id
  buffer = strsplit(sectordata$id,split="|",fixed=T)
  buffer = data.frame(t(sapply(buffer,c)))
  sectordata$recipientname = buffer$X1
  sectordata$parent = optmimize_sectorgroup_labels(buffer$X2)
  
  # compute the data at the sectorgroup level (middle level)
  sectorgroupdata = aggregate(financial_in_mio ~ sector_group, data = dfwide, sum)
  names(sectorgroupdata) = c("label","value")
  sectorgroupdata$label = optmimize_sectorgroup_labels(sectorgroupdata$label)
  sectorgroupdata$id = sectorgroupdata$label
  # set the parent to Afrika if no country was chosen
  if (country=="Afrika") {
    sectorgroupdata$recipientname = rep("Afrika",length(sectorgroupdata$id))
  } else {
    sectorgroupdata$recipientname = rep(sectordata$recipientname[1],length(sectorgroupdata$id))
  }
  sectorgroupdata$parent = sectorgroupdata$recipientname
  
  # compute the data at the recipient level (highest level)
  # if no country was chosen, add label "Afrika"
  if (country=="Afrika") {
    recipientdata = data.frame(label = "Afrika",
                               value = sum(dfwide$financial_in_mio),
                               id = "Afrika",
                               recipientname = "Afrika",
                               parent = '')
  } else {
    recipientdata = data.frame(label = dfwide$country_en[1],
                              value = sum(dfwide$financial_in_mio),
                              id = dfwide$country_en[1],
                              recipientname = dfwide$country_en[1],
                              parent = '')
    
  }
  # combine all 3 levels into a single long form df for treemap plotting function
  data = rbind(recipientdata,sectorgroupdata,sectordata)
  
}

# data = subset(data_bmz_group, country_en=='Algeria')
# data$financial_in_mio = round(data$grant_equiv_in_mio, 2)
# # pick the roman numerals out ofthe sector name
# data$sector_group = sapply(strsplit(data$sector_name, split='.', fixed=TRUE), `[`, 1) 
# 
# # get relevant columns
# dfwide = data[c("country_en","sector_group","sector_name","financial_in_mio")]
# dfwide$id = paste(dfwide$country_en,dfwide$sector_group,dfwide$sector_name, sep="|")
# # compute the data at the sector level (lowest level)
# sectordata = aggregate(financial_in_mio ~ id, data = dfwide, sum)
# names(sectordata) = c("id","value")
# sectordata$label = sectordata$id
# buffer = strsplit(sectordata$id,split="|",fixed=T)
# buffer = data.frame(t(sapply(buffer,c)))
# sectordata$recipientname = buffer$X1
# sectordata$parent = buffer$X2
# # compute the data at the sectorgroup level (middle level)
# sectorgroupdata = aggregate(financial_in_mio ~ sector_group, data = dfwide, sum)
# names(sectorgroupdata) = c("label","value")
# sectorgroupdata$id = sectorgroupdata$label
# sectorgroupdata$recipientname = rep(sectordata$recipientname[1],length(sectorgroupdata$id))
# sectorgroupdata$parent = sectorgroupdata$recipientname
# # compute the data at the recipient level (highest level)
# recipientdata = data.frame(label = dfwide$country_en[1],
#                            value = sum(dfwide$financial_in_mio),
#                            id = dfwide$country_en[1],
#                            recipientname = dfwide$country_en[1],
#                            parent = '')
# 
# dflong = rbind(recipientdata,sectorgroupdata,sectordata)
# 

##################################
# ODA time series chart data prep 
##################################
# @Lea: this function takes data_bmz_group as input and prepares data such that
# the sector groups (roman numerals) can be plotted directly in server.R

# only used for testing purposes
#data = subset(data_bmz_group, country_en=='Algeria')
#data=data_bmz_group
#data$financial_in_mio = round(data$grant_equiv_in_mio, 2)
oda_timeseries_chart_data_prep <- function(data, country) {
  
    # pick the roman numerals out of the sector name to get sector_group variable
    data$sector_group = sapply(strsplit(data$sector_name, split='.', fixed=TRUE), `[`, 1) 
    
    # get the sum of fincncial_in_mio for each year and sector_group
    sectorgroupdata = aggregate(financial_in_mio ~ sector_group + year, data = data, sum)
    
    # for plotting purposes, we need to add the country name to the df
    if (country=="Afrika") {
      sectorgroupdata$country_en = rep("Afrika",length(sectorgroupdata$financial_in_mio))
    } else {
      sectorgroupdata$country_en = rep(data$country_en[1],length(sectorgroupdata$financial_in_mio))
    }
    
    # return country
    data = sectorgroupdata
    
}
  
  



###################
#Area Chart data prep
###################
# still used for femEZ
area_chart_data_prep <- function(data, group_by_var) {
  
  data %>%
    dplyr::group_by({{group_by_var}}, year) %>% 
    
    #sum
    #dplyr::summarise(financial_commitment_in_mio_per_variable = sum(financial_commitment_in_mio)) %>% 
    dplyr::summarise(financial_in_mio_per_variable = sum(financial_in_mio)) %>% 
    
    # #pivot for plotting
    tidyr::pivot_wider(names_from = {{group_by_var}},
                       #values_from = financial_commitment_in_mio_per_variable,
                       values_from = financial_in_mio_per_variable)
}


####################
#Run App on shiny
###################

# rsconnect::setAccountInfo(name = token$name,
#                            token = token$token,
#                            secret = token$secret)

